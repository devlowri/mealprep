import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Image, Dimensions, ScrollView } from 'react-native';
import { Overlay } from 'react-native-elements';
import { Icon } from 'react-native-elements';
import styles from './styles';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import Loader from '../loader';
import config from '../../config/config.json';

const windowWidth = Dimensions.get('window').width-50;

export default class RecipeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            overlay: false,
            image: this.props.image,
            title: this.props.title,
            ingredients: [
                ...this.props.ingredients
            ],
            instructions: [
                ...this.props.instructions
            ],
            dificulty: this.props.dificulty
        }
    }

    render() {
        let stars = [];
        for (let index = 0; index < 5; index++) {
            stars.push(index < this.state.dificulty ? <AntDesignIcon name="star" color="white" size={10} key={index}/> : <AntDesignIcon name="staro" color="white" size={10} key={index}/>)
        }
        const overlay = (
            <Overlay
                isVisible={this.state.overlay}
                onBackdropPress={() => this.setState({overlay:false})}
                overlayStyle={styles.overlayStyle}
            >
                <View style={{flex: 0.9, width: windowWidth, margin: -10}}>
                    <Image
                        source={{uri: this.state.image}}
                        style={styles.overlayImage}
                    />
                    <ScrollView>
                        <Text style={styles.title}>{this.state.title}</Text>
                        <Text style={styles.subtitle}>Ingredientes</Text>
                        {
                            this.state.ingredients.map((ingredient,id) => {
                                return (
                                    <Text style={styles.ingredient} key={'ingredient'+id}>
                                        - {ingredient}
                                    </Text>
                                );
                            })
                        }
                        <Text style={styles.subtitle}>Instrucciones</Text>
                        {
                            this.state.instructions.map((instruction,id) => {
                                return (
                                    <Text style={styles.instruction} key={'instruction'+id}>
                                        {instruction}
                                    </Text>
                                );
                            })
                        }
                    </ScrollView>
                </View>
                <View style={styles.closeButton}>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => this.setState({overlay: false})}>
                        <Icon name='close' type='material' color={config.colors.primaryColor} size={30} reverse raised />
                    </TouchableOpacity>
                </View>
            </Overlay>
        );

        return (
            <TouchableOpacity activeOpacity={0.6} onPress={() => this.setState({overlay: true})}>
                <ImageBackground
                    source={{uri: this.state.image}}
                    style={styles.recipe}
                    imageStyle={{ borderRadius: 10 }}
                    onLoadEnd={(e) => {this.setState({loading: false})}}
                >
                    {this.state.loading ?
                        <Loader color='white'/>
                    :
                        <View style={styles.buttonContainer}>
                            {overlay}
                            <Text style={styles.recipeText} numberOfLines={1}>
                                {this.state.title}
                            </Text>
                            <View style={{flexDirection: 'row'}}>
                                {stars}
                            </View>
                        </View>
                }
                </ImageBackground>
            </TouchableOpacity>
        );
    }
}