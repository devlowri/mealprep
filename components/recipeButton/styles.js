import { StyleSheet, Dimensions } from 'react-native';
import config from '../../config/config.json';

const windowWidth = Dimensions.get('window').width-50;

export default StyleSheet.create({
    recipe: {
        width: windowWidth, height: 150,
        borderRadius: 50,
        resizeMode: "cover",
        marginBottom: 20
    },
    buttonContainer: { position: 'absolute', bottom: 5, padding: 10 },
    recipeText: { 
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
        flexDirection: 'column'
    },
    overlayStyle: { borderRadius: 20 },
    overlayImage: { width: windowWidth, height: 150, borderTopLeftRadius: 20, borderTopRightRadius: 20, resizeMode: 'cover' },
    title: { padding: 10, textAlign: 'center', fontSize: 20, fontWeight: 'bold', color: config.colors.claret},
    subtitle: { padding: 10, textAlign: 'left', fontSize: 18, fontWeight: 'bold', color: config.colors.claret},
    ingredient: {paddingLeft: 20, textAlign: 'left', fontSize: 14 },
    instruction: {padding: 10, paddingTop: 0, textAlign: 'left', fontSize: 14},
    closeButton: { position: 'absolute', bottom: -25, width: windowWidth, alignItems: 'center'}
});