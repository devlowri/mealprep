import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import config from '../../config/config.json';

const Loader = (props) => {
    const colors = config.colors;

    return (
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
            <ActivityIndicator size={100} color={props.color || colors.primaryColor}/>
        </View>
    );
}

export default Loader;