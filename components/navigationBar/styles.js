import { StyleSheet } from 'react-native';
import config from '../../config/config.json';

export default StyleSheet.create({
  container: {
    position: "absolute",
    bottom: 0,
    left: 0,
    backgroundColor: config.colors.primaryColor,
    height: config.navigationBarHeight,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection:'row',
    borderStyle: 'solid'
  }
});