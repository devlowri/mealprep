import React, {Component} from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './styles';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import { useNavigation } from '@react-navigation/native';

const NavigationBar = (props) => {
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
                <AntDesignIcon name="home" color="white" size={30} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Recipe')}>
                <EntypoIcon name="bowl" color="white" size={30} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                <AntDesignIcon name="user" color="white" size={30} />
            </TouchableOpacity>
        </View>
    );
}

export default NavigationBar;