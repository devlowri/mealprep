import { StyleSheet, Dimensions } from 'react-native';
import config from '../../config/config.json';

const bottomDisplacement = 15;

export default StyleSheet.create({
  backgroundImage: { flex: 1, resizeMode: 'cover' },
  scrollView: { flex: 1, padding: 15, backgroundColor: "rgba(0,0,0,0)"},
  content: { flex: 1, flexDirection: 'column', justifyContent: 'space-around'},
  logo: { width: 100, height: 100, opacity: 0.8, alignSelf: 'center', marginBottom: 50 },
  inputsContainer: { width: '85%', alignSelf: 'center' },
  inputLabel: { color: config.colors.flamingo },
  textInput: {padding: 0, borderBottomColor: config.colors.flamingo, borderBottomWidth: 1, fontSize: 18, color: config.colors.secondaryText, paddingTop: 5, paddingBottom: 5, marginBottom: bottomDisplacement},
  rulesAdvice: {color: config.colors.secondaryText, fontSize: 10, marginBottom: 10},
  modal: { width: "90%" }
});