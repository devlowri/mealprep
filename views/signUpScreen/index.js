import React from 'react';
import styles from './styles';
import { ImageBackground, View, Text, ScrollView, Image, TextInput } from 'react-native';
import { Button } from 'react-native-paper';
import config from '../../config/config.json';
import auth from '@react-native-firebase/auth';

import Loader from '../../components/loader';
import ScrollMargin from '../../components/scrollMargin';
import { useNavigation } from '@react-navigation/native';
import { Overlay } from 'react-native-elements';

export default class SignUpScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            overlay: false,
            error: '',
            name: 'Antoni',
            email: 'antoni@hotmail.com',
            password: 'petunia72',
        }
    }

    createAccount = (navigation) => {
        // Regex and alert message variables
        var message = '';
        var nameReg = /^[A-zÀ-ú ]*$/;
        var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        // Validate data...
        if(!nameReg.test(String(this.state.name).toLowerCase())) message+="El nombre contiene carácteres especiales o números.\n"
        if(this.state.name.length<=2||this.state.name.length>24) message+="El nombre contiene menos de 3 carácteres o más de 24.\n"
        if(!emailReg.test(String(this.state.email).toLowerCase())) message+="El correo electrónico introducido no es válido.\n"
        if(this.state.email.length>=254) message+="El correo electrónico introducido es demasiado largo.\n"
        if(this.state.password.length<6) message+="La contraseña debe contener mínimo 6 carácteres.\n"
        if(message!='') {
            this.setState({overlay:true, error: message});
            return;
        }

        // If everything is fine then create the account and set the display name and default avatar image
        auth()
        .createUserWithEmailAndPassword(this.state.email, this.state.password)
        .then(() => {
            // If the account creation its successful then modify the user profile and go back to profile screen
            var user = auth().currentUser;

            user.updateProfile({
                displayName: this.state.name,
                photoURL: "https://i.imgur.com/1tVQruH.jpg"
            }).then(function() {
                console.log("Profile updated.")
                navigation.navigate("Home");
            }).catch(function(error) {
                console.log(error)
            });
        })
        .catch(error => {
            if (error.code === 'auth/email-already-in-use') {
                this.setState({overlay:true, error: "Ya hay una cuenta asociada al correo " + this.state.email + ".\n"});
                return;
            }

            if (error.code === 'auth/invalid-email') {
                this.setState({overlay:true, error: "El correo " + this.state.email + " es inválido.\n"});
                return;
            }
        });
    }

    render() {
        const colors = config.colors;

        return(
            <ImageBackground
                source={require('../../assets/signupbackground.jpg')}
                style={styles.backgroundImage}
                onLoadEnd={(e) => {this.setState({loading: false})}}>
                {this.state.loading ?
                    <Loader/>
                :
                    <ScrollView style={styles.scrollView} contentContainerStyle={{flexGrow: 1}}>
                        <View style={styles.content}>
                            <Overlay isVisible={this.state.overlay} onBackdropPress={() => this.setState({overlay:false, error:''})}>
                                <View style={styles.modal}>
                                    <Text style={{textAlign: 'center'}}>{this.state.error}</Text>
                                    <Button
                                        mode={'contained'}
                                        color={colors.white}
                                        style={{width: "100%", alignSelf: 'center', borderRadius: 100}}
                                        contentStyle={{height: 50, backgroundColor: colors.flamingo}}
                                        labelStyle={{color: colors.white, fontSize: 13}}
                                        onPress={() => this.setState({overlay:false, error:''})}>
                                        Entendido
                                    </Button>
                                </View>
                            </Overlay>
                            <View style={styles.inputsContainer}>
                                <Image style={styles.logo} source={require('../../assets/logoflamingo.png')} />
                                <Text style={styles.inputLabel}>NOMBRE</Text>
                                <TextInput
                                    value={this.state.name}
                                    onChangeText={name => this.setState({name})}
                                    style={styles.textInput}
                                />
                                <Text style={styles.rulesAdvice}>* El nombre no debe contener números o carácteres especiales.</Text>

                                <Text style={styles.inputLabel}>EMAIL</Text>
                                <TextInput
                                    value={this.state.email}
                                    onChangeText={email => this.setState({email})}
                                    style={styles.textInput}
                                />

                                <Text style={styles.inputLabel}>CONTRASEÑA</Text>
                                <TextInput
                                    secureTextEntry
                                    value={this.state.password}
                                    onChangeText={password => this.setState({password})}
                                    style={styles.textInput}
                                />
                                <Text style={styles.rulesAdvice}>* La contraseña debe contener un mínimo de 6 carácteres.</Text>

                                <Button
                                    mode={'contained'}
                                    color={colors.white}
                                    style={{width: "100%", alignSelf: 'center', borderRadius: 100, margin: 5, marginTop: 30}}
                                    contentStyle={{height: 50, backgroundColor: colors.white}}
                                    labelStyle={{color: colors.flamingo, fontSize: 13}}
                                    onPress={() => this.createAccount(this.props.navigation)}>
                                    Crear cuenta
                                </Button>
                            </View>
                        </View>
                        <ScrollMargin/>
                    </ScrollView>
                }
            </ImageBackground>
        );
    }
}