import React, { Component } from 'react';
import { Text, View, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { SearchBar, Icon } from 'react-native-elements';
import firestore from '@react-native-firebase/firestore';
import ScrollMargin from '../../components/scrollMargin';
import config from '../../config/config.json';
import styles from './styles';
import RecipeButton from '../../components/recipeButton';
import Loader from '../../components/loader';

export default class RecipeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            tabs: ['Todos', 'Desayuno', 'Plato principal', 'Aperitivo', 'Postre', 'Batidos'],
            activeTab: 'Todos',
            data: null
        }
    }

    async getMarker() {
        let collection = [];
        const recipesCollection = await firestore().collection('recipes').get()
        recipesCollection.docs.map(doc => {
            collection.push(doc.data());
        });
        this.setState({data: collection})
    }

    componentDidMount() {
        this.getMarker();
    }

    updateSearch = search => {
        this.setState({ search });
    };

    tabButton = (label,id) => {
        let active = label===this.state.activeTab ? {color: config.colors.mainText, fontWeight: 'bold' } : null;
        return (
            <TouchableOpacity onPress={() => this.setState({activeTab: label})} key={id}>
                <Text style={[styles.tabValue, active]}>
                    {label}
                </Text>
            </TouchableOpacity>
        )
    }

    renderButton = () => {
        return this.state.data.map((data) => {
            return <RecipeButton
                title={data.title}
                image={data.image}
                ingredients={data.ingredients}
                instructions={data.instructions}
                dificulty={data.dificulty} />
        });
    }

    render() {
        const { search } = this.state;
        
        if(!this.state.data) return <Loader/>;

        const addIcon = (
            <TouchableOpacity activeOpacity={0.7}>
                <Icon name='add' type='material' color={config.colors.flamingo} size={26} reverse raised />
            </TouchableOpacity>
        );

        const searchBar = (
            <SearchBar
                placeholder="Busca aquí..." onChangeText={this.updateSearch}
                value={search} inputStyle={{backgroundColor: config.colors.claret, color: 'white'}}
                containerStyle={{backgroundColor: config.colors.primaryColor, borderBottomColor: 'transparent', borderTopColor: 'transparent'}}
                placeholderTextColor={"rgba(255,255,255,0.5)"} inputContainerStyle={{backgroundColor: config.colors.claret, borderWidth: 0}}
                searchIcon={{color:'white'}} clearIcon={{color:'white'}} cancelIcon={{color:'white'}}
            />
        );

        return (
            <ImageBackground source={require('../../assets/signupbackground.jpg')} style={styles.backgroundImage}>
                    <ScrollView>
                        <View>
                            {searchBar}
                            <View style={styles.content}>
                                <View style={styles.typesContainer}>
                                    <ScrollView horizontal style={styles.typeTab} showsHorizontalScrollIndicator={false}>
                                        <View style={styles.textTabs}>
                                            {this.state.tabs.map((tab,id) => {
                                                return this.tabButton(tab,id);
                                            })}
                                        </View>
                                    </ScrollView>
                                    <View style={styles.addTab}>
                                        {addIcon}
                                    </View>
                                </View>
                                <ScrollView>
                                    <View style={styles.recipes}>
                                        {this.renderButton()}
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                    </ScrollView>
            </ImageBackground>
        )
    }
}