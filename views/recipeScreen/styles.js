import { StyleSheet, Dimensions } from 'react-native';
import config from '../../config/config.json';

const windowWidth = Dimensions.get('window').width/1.5;

export default StyleSheet.create({
  content: { flexGrow: 1, paddingHorizontal: 10},
  backgroundImage: { flex: 1, resizeMode: 'cover' },
  typesContainer: { flexDirection: 'row', width: '100%', marginBottom: 20 },
  typeTab: { flexDirection: 'row' },
  textTabs: { flexDirection: 'row', justifyContent: 'center', textAlignVertical: 'center', alignItems: 'center' },
  addTab: { width: 70 },
  tabValue: { fontSize: 16, marginRight: 20, color: config.colors.secondaryText},
  recipes: { alignItems: 'center'}
});