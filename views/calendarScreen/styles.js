import { StyleSheet } from 'react-native';
import config from '../../config/config.json';

export default StyleSheet.create({
  screen: { flex:1, backgroundColor: 'white'}
});