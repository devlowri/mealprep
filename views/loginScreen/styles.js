import { StyleSheet } from 'react-native';
import config from '../../config/config.json';

const bottomDisplacement = 15;

export default StyleSheet.create({
    scrollView: { flex: 1, padding: 15, backgroundColor: "rgba(0,0,0,0)"},
    backgroundImage: { flex: 1, resizeMode: 'cover' },
    logo: { width: 100, height: 100, opacity: 0.8, alignSelf: 'center' },
    content: { flex: 1, flexDirection: 'column', justifyContent: 'space-around'},
    inputsContainer: { width: '85%', alignSelf: 'center' },
    inputLabel: { color: config.colors.flamingo },
    textInput: {padding: 0, borderBottomColor: config.colors.flamingo, borderBottomWidth: 1, fontSize: 18, color: config.colors.secondaryText, paddingTop: 5, paddingBottom: 5, marginBottom: bottomDisplacement},
    forgotPassword: { color: 'white', alignSelf: 'flex-end', fontWeight: 'bold', marginBottom: bottomDisplacement },
    socialAltContainer: { flexDirection: 'row' },
    socialAltText: {fontSize: 12, color: config.colors.secondaryText, textAlign: 'center', marginTop: 17, height: 35},
    socialAltBox: { width: '20%' },
    socialAltBorder: {marginBottom: 25, borderBottomColor: config.colors.secondaryText, borderBottomWidth: 1,}
});