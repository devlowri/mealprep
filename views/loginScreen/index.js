import React from 'react';
import styles from './styles';
import config from '../../config/config.json';
import { ImageBackground, Text, View, ScrollView, Image, TextInput } from 'react-native';
import { Button } from 'react-native-paper';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import auth from '@react-native-firebase/auth';

import Loader from '../../components/loader';
import ScrollMargin from '../../components/scrollMargin';

export default class LogInScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            email: 'petunia@gmail.com',
            password: 'petunia72'
        }
    }

    logIn = (email, password, navigation) => {
        auth()
        .signInWithEmailAndPassword(email,password)
        .then(() => {
            console.log("Login correcto");
            navigation.navigate("Profile");
        })
        .catch(function(error) {
            console.log('LOGIN ERROR');
        });
    }

    /*async onGoogleButtonPress() {
        // Get the users ID token
        const idToken = await GoogleSignin.signIn();
        // Create a Google credential with the token
        const googleCredential = auth.GoogleAuthProvider.credential(idToken);
        console.log("googleCredential:",googleCredential)
        // Sign-in the user with the credential
        return auth().signInWithCredential(googleCredential);
    }*/

    render() {
        const colors = config.colors;

        return(
            <ImageBackground
                source={require('../../assets/signupbackground.jpg')}
                style={styles.backgroundImage}
                onLoadEnd={(e) => {this.setState({loading: false})}}>
                {this.state.loading ?
                    <Loader/>
                :
                    <ScrollView style={styles.scrollView} contentContainerStyle={{flexGrow: 1}} >
                        <View style={styles.content}>
                            <Image style={styles.logo} source={require('../../assets/logoflamingo.png')} />
                            <View style={styles.inputsContainer}>
                                <Text style={styles.inputLabel}>EMAIL</Text>
                                <TextInput
                                    value={this.state.email}
                                    onChangeText={email => this.setState({email})}
                                    style={styles.textInput}
                                />
                                <Text style={styles.inputLabel}>CONTRASEÑA</Text>
                                <TextInput
                                    secureTextEntry
                                    value={this.state.password}
                                    onChangeText={password => this.setState({password})}
                                    style={styles.textInput}
                                />
                                <Text style={styles.forgotPassword}>Recuperar contraseña</Text>
                                <Button
                                    mode={'contained'}
                                    color={colors.white}
                                    style={{width: "100%", alignSelf: 'center', borderRadius: 100, margin: 5}}
                                    contentStyle={{height: 50, backgroundColor: colors.white}}
                                    labelStyle={{color: colors.flamingo, fontSize: 13}}
                                    onPress={() => this.logIn(this.state.email, this.state.password, this.props.navigation)}>
                                    Iniciar sesión
                                </Button>
                                {/*}
                                <View style={styles.socialAltContainer}>
                                    <View style={[styles.socialAltBox, styles.socialAltBorder]}/>
                                    <View style={[styles.socialAltBox,{width: '60%'}]}>
                                        <Text style={styles.socialAltText}>INICIAR SESIÓN CON:</Text>
                                    </View>
                                    <View style={[styles.socialAltBox, styles.socialAltBorder]}/>
                                </View>

                                <Button
                                    mode={'contained'}
                                    color={colors.flamingo}
                                    style={{width: "100%", alignSelf: 'center', borderRadius: 100, margin: 5}}
                                    contentStyle={{height: 50, backgroundColor: colors.flamingo}}
                                    labelStyle={{color: colors.white, fontSize: 13}}>
                                    <AntDesignIcon name="google" color="white" size={15} /> Google
                                </Button>*/}
                            </View>
                        </View>
                        <ScrollMargin/>
                    </ScrollView>
                }
            </ImageBackground>
        );
    }
}