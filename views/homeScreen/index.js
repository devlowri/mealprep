import React, { Component } from 'react';
import { Text, View, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import ScrollMargin from '../../components/scrollMargin';
import styles from './styles';
import config from '../../config/config.json';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import FeatherIcon from 'react-native-vector-icons/Feather';
import Loader from '../../components/loader';

const iconSize = Math.round(Dimensions.get('window').width)/6;
const colors = config.buttonsColors;

export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = { loading: true }
        const loader = setTimeout(() => {
            this.setState({loading: false})
        }, 500);
    }

    componentDidMount() {
        clearTimeout(this.loader);
    }
    
    render() {
        if(this.state.loading) return <Loader />

        return (
            <View style={styles.screen}>
                <View style={styles.menuContainer}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Recipe')}>
                        <View style={[styles.button, {backgroundColor: colors.recipes}]}>
                            <EntypoIcon name="bowl" color={config.colors.white} size={iconSize} />
                            <Text style={styles.buttonText}>Recetas</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Calendar')}>
                        <View style={[styles.button, {backgroundColor: colors.calendar}]}>
                            <AntDesignIcon name="calendar" color={config.colors.white} size={iconSize+2} />
                            <Text style={styles.buttonText}>Calendarios</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Lunchbox')}>
                        <View style={[styles.button, {backgroundColor: colors.tupper}]}>
                            <EntypoIcon name="box" color={config.colors.white} size={iconSize+2} />
                            <Text style={styles.buttonText}>Fiambreras</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                        <View style={[styles.button, {backgroundColor: colors.profile}]}>
                            <AntDesignIcon name="user" color={config.colors.white} size={iconSize+2} />
                            <Text style={styles.buttonText}>Perfil</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={styles.fillBottom}>
                    <View style={[styles.lastButton, {backgroundColor: colors.more}]}>
                        <FeatherIcon name="settings" color={config.colors.white} size={iconSize+2} />
                        <Text style={styles.buttonText}>Pronto más actualizaciones...</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}