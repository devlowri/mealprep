import { StyleSheet, Dimensions } from 'react-native';
import config from '../../config/config.json';

const screenWidth = Math.round(Dimensions.get('window').width)/2;

export default StyleSheet.create({
  screen: { flex:1, backgroundColor: 'white'},
  menuContainer: { flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row' },
  button: {width: screenWidth, height: screenWidth, justifyContent: 'center', alignItems: 'center' },
  buttonText: {fontSize: 16, color: config.colors.white, marginTop: 5, padding: 10, textAlign: 'center', fontWeight: 'bold'},
  buttonDescription: { fontSize: 12, color: config.colors.white},
  fillBottom: {width: '100%', flex: 1, backgroundColor: 'rgba(0,0,0,0)', paddingBottom: config.navigationBarHeight},
  lastButton: {width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'}
});