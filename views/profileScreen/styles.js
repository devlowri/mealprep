import { StyleSheet } from 'react-native';
import config from '../../config/config.json';

export default StyleSheet.create({
  scrollView: { flex: 1, padding: 15, backgroundColor: "rgba(0,0,0,0)"},
  backgroundImage: { flex: 1, resizeMode: 'cover' },
  logo: { width: 250, height: 250, alignSelf: 'center' },
  content: { flex: 1, flexDirection: 'column', justifyContent: 'space-around'},
  profileContent: { flexDirection: 'column', justifyContent: 'space-around', width: '85%', backgroundColor: config.colors.white, alignSelf: 'center',
  padding: 20, borderRadius: 20},
  profilePhoto: { height: 150, width: 150, borderRadius: 100, alignSelf: 'center', borderWidth: 2, borderColor: config.colors.flamingo},
  divider: {borderBottomColor: config.colors.flamingo, borderBottomWidth: 2, marginVertical: 25},
  userName: { fontSize: 25, alignSelf: 'flex-start', marginBottom: 25 },
  userEmail: { fontSize: 15},
  iconText: { flexDirection: 'row', marginBottom: 25 },
  iconMargin: { marginRight: 10 }
});