import React, { Component } from 'react';
import { Text, View, ScrollView, Image, ImageBackground, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Button } from 'react-native-paper';
import styles from './styles';
import config from '../../config/config.json';
import FontistoIcon from 'react-native-vector-icons/Fontisto';
import auth from '@react-native-firebase/auth';

import ScrollMargin from '../../components/scrollMargin';
import Loader from '../../components/loader';

export default class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }

    createAccount = (email, password) => {
        auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
            console.log('User account created & signed in!');
        })
        .catch(error => {
            if (error.code === 'auth/email-already-in-use') {
            console.log('That email address is already in use!');
            }

            if (error.code === 'auth/invalid-email') {
            console.log('That email address is invalid!');
            }
            console.error(error);
        });
    }

    logOut = () => {
        auth()
        .signOut()
        .then(() => console.log('User signed out!'));
    }

    profileButtons = (text, mode, backgroundColor, textColor, borderColor, action, width='80%') => {
        return (
            <Button
                mode={mode}
                color={config.colors.white}
                style={{width, alignSelf: 'center', borderRadius: 100, margin: 5, borderColor, borderWidth: borderColor ? 1.5 : 0}}
                contentStyle={{height: 50, backgroundColor}}
                labelStyle={{color: textColor, fontSize: 13}}
                onPress={action}>
                {text}
            </Button>
        );
    }

    render() {
        const colors = config.colors;

        if(!this.props.user) {
            return (
                <ImageBackground
                    source={require('../../assets/appbackground.jpg')}
                    style={styles.backgroundImage}
                    onLoadEnd={(e) => {this.setState({loading: false})}}>
                    {this.state.loading ?
                        <Loader/>
                    :
                        <ScrollView style={styles.scrollView} contentContainerStyle={{flexGrow: 1}} >
                            <View style={styles.content}>
                                <Image
                                    style={styles.logo}
                                    source={require('../../assets/logotitle.png')}
                                />
                                <View>
                                    {this.profileButtons('Registrarse', 'outlined', '', colors.white, colors.white, () => this.props.navigation.navigate('SignUp'))}
                                    {this.profileButtons('Iniciar sesión', 'contained', colors.white, colors.flamingo, '', () => this.props.navigation.navigate('Login'))}
                                </View>
                            </View>
                            <ScrollMargin/>
                        </ScrollView>
                    }
                </ImageBackground>
            )
        } else {
            var user = auth().currentUser;
            return (
                <ImageBackground
                    source={require('../../assets/signupbackground.jpg')}
                    style={styles.backgroundImage}
                    onLoadEnd={(e) => {this.setState({loading: false})}}>
                    {this.state.loading ?
                        <Loader/>
                    :
                        <ScrollView style={styles.scrollView} contentContainerStyle={{flexGrow: 1}} >
                            <View style={styles.profileContent}>
                                <Image style={styles.profilePhoto} source={{uri: user.photoURL}}/>
                                <View style={styles.divider}/>
                                <Text style={styles.userName}>{user.displayName}</Text>
                                <View style={styles.iconText}>
                                    <FontistoIcon name="email" color={config.colors.flamingo} size={20} style={styles.iconMargin} />
                                    <Text style={styles.userEmail}>{user.email}</Text>
                                </View>
                                    {this.profileButtons('Cerrar sesión', 'contained', colors.flamingo, colors.white, '', () => this.logOut(), '100%')}
                            </View>
                            <ScrollMargin/>
                        </ScrollView>
                    }
                </ImageBackground>
            )
        }
    }
}