import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import {View, StatusBar} from 'react-native';
import config from './config/config.json';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import auth from '@react-native-firebase/auth';

import HomeScreen from './views/homeScreen';
import RecipeScreen from './views/recipeScreen';
import ProfileScreen from './views/profileScreen';
import SignUpScreen from './views/signUpScreen';
import LoginScreen from './views/loginScreen';
import CalendarScreen from './views/calendarScreen';
import LunchboxScreen from './views/lunchboxScreen';
import NavigationBar from './components/navigationBar';

const Stack = createStackNavigator();

export default function App() {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  function Options(titleName) {
    return ({
      title: titleName,
      headerStyle: {
        backgroundColor: config.colors.primaryColor
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      },
      headerTitleAlign: 'center'
    })
  }

  function Home({ navigation }) {
    return (
      <View style={{ flex: 1 }}>
        <HomeScreen user={user} navigation={navigation}/>
        <NavigationBar/>
      </View>
    );
  }

  function Profile({ navigation }) {
    return (
      <View style={{ flex: 1 }}>
        <ProfileScreen user={user} navigation={navigation}/>
        <NavigationBar/>
      </View>
    );
  }

  function Recipe({ navigation }) {
    return (
      <View style={{ flex: 1 }}>
        <RecipeScreen user={user} navigation={navigation}/>
      </View>
    );
  }

  function SignUp({ navigation }) {
    return (
      <View style={{ flex: 1 }}>
        <SignUpScreen user={user} navigation={navigation}/>
        <NavigationBar/>
      </View>
    );
  }

  function Login({ navigation }) {
    return (
      <View style={{ flex: 1 }}>
        <LoginScreen user={user} navigation={navigation}/>
        <NavigationBar/>
      </View>
    );
  }

  function Calendar({ navigation }) {
    return (
      <View style={{ flex: 1 }}>
        <CalendarScreen user={user} navigation={navigation}/>
        <NavigationBar/>
      </View>
    );
  }
  function Lunchbox({ navigation }) {
    return (
      <View style={{ flex: 1 }}>
        <LunchboxScreen user={user} navigation={navigation}/>
        <NavigationBar/>
      </View>
    );
  }

  return (
    <NavigationContainer>
      <StatusBar hidden />
      <Stack.Navigator initialRouteName="Home" /*screenOptions={{headerShown: false}}*/>
        <Stack.Screen name="Home" component={Home} options={{headerShown: false}}/>
        <Stack.Screen name="Recipe" component={Recipe} options={Options("Recetas")}/>
        <Stack.Screen name="Profile" component={Profile} options={user ? Options("Perfil") : {headerShown: false}}/>
        <Stack.Screen name="SignUp" component={SignUp} options={{headerShown: false}}/>
        <Stack.Screen name="Login" component={Login} options={{headerShown: false}}/>
        <Stack.Screen name="Calendar" component={Calendar} options={Options("Calendarios")}/>
        <Stack.Screen name="Lunchbox" component={Lunchbox} options={Options("Fiambreras")}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};
